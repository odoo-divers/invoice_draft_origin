{
    'name' : 'invoice_draft_origin',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'account',
    ],
    'data' : [
    		'invoice_draft_origin_view.xml',
    ],

    'installable' : True,
    'application' : False,
}
